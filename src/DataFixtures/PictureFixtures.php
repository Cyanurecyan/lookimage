<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Mmo\Faker\PicsumProvider;
use Symfony\Component\HttpFoundation\File\File;
use Faker;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * Permet de dire à notre fixtures si elle dépend d'autres fixtures
     */
    public function getDependencies() 
    {
        return [
            CategorieFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        // Instancier Faker pour pouvoir l'utiliser
        // Par défaut : données en anglais, paramétrable
        $faker = Faker\Factory::create('fr_FR');

        // On ajoute le bundle "Mmo Faker-image" à Faker
        $faker->addProvider(new PicsumProvider($faker));

        // Création d'une boucle for() pour choisir le nombre d'éléments mis en BDD
        for ($i=0; $i <= 100; $i++) { 

            // Spécifier le chemin du dossier d'upload
            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152, 2312), random_int(864, 1736));

            // Récupération d'une référence aléatoirement.
            // On récupère un objet de l'entité catégorie généré dans le fichier CategorieFixtures
            // grace au nom choisi lors de l'enregistrement dans les références.
            $category = $this->getReference('category_'. random_int(0, 10));
            $user = $this->getReference('user_' . random_int(0,10));

            $picture = new Picture();

            // Utilisation des setters
            $picture->setDescription($faker->sentence(26));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('- 4 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('- 3 years'));
            $picture->setCategory($category);
            $picture->setUser($user);

            // Gestion de l'image
            $picture->setImageFile(new File($image));
            $picture->setImage(str_replace('./public/uploads/images/photos\\', '', $image));

            // Garde de côté les données en attendant l'exécution des requêtes
            $manager->persist($picture);
        }

        $manager->flush();
    }
}
