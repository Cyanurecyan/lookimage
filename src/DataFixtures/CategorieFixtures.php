<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Faker;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Instancier Faker pour pouvoir l'utiliser
        // Par défaut : données en anglais, paramétrable
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for() pour choisir le nombre d'éléments mis en BDD
        for ($i=0; $i <= 10; $i++) { 
            $category = new Categorie();

            // Utilisation des setters
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

            // Enregistre l'objet dans une référence. 
            // Cad qu'on pourra utiliser les objets enregistrés dans une autre fixture 
            // afin de pouvoir effectuer des relations de table.
            // le premier paramètre est un nom qui se doit d'être unique.
            // le second paramètre est l'objet qui sera lié à ce nom.
            $this->addReference('category_'.$i, $category);

            // Garde de côté les données en attendant l'exécution des requêtes
            $manager->persist($category);
        }

        $manager->flush();
    }
}
