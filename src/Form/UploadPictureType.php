<?php

namespace App\Form;

use App\Entity\Picture;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UploadPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Sélectionner une photo :',
                // Options de VichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'imagine_pattern' => 'thumbnail',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir une photo.',
                        'groups' => ['new_picture']
                    ]),
                    new Image([
                        'maxSize' => '5M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 5Mo.',
                        'mimeTypes' => ['image/gif', 'image/png', 'image/jpeg', 'image/webp'],
                        'mimeTypesMessage' => 'Cette image est invalide.',
                        'groups' => ['new_picture']
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Description de la photo :',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une description.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('tags', TextType::class, [
                'required' => true,
                'label' => 'Tags :',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un ou plusieurs tags.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('category', EntityType::class, [
                'required' => true,
                'label' => 'Catégorie de la photo :',
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
