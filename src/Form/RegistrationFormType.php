<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Prénom', // label du champ
                // 'attr' => [                  // pour gérer les attributs
                //     'placeholder' => 'Jean',
                //     'class' => 'maSuperClass',
                // ]
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre prénom.'
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Nom de famille', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre nom.'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Email', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre email doit être renseigné'
                    ]),
                    new Email([
                        'message' => 'Votre email doit être au format monmail@mail.com'
                    ])
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => 'En soumettant ce formulaire, j’accepte que LookImage conserve mes  données personnelles via ce formulaire. Aucune exploitation commerciale ne sera faite des données conservées.', // label du champ
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les RGPD.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Renseignez un mot de passe.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
