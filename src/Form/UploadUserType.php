<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class UploadUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Email :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre email doit être renseigné'
                    ]),
                    new Email([
                        'message' => 'Votre email doit être au format monmail@mail.com'
                    ])
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Utilisateur' => 'ROLE_USER',
                    'Editeur' => 'ROLE_EDITOR',
                    'Administrateur' => 'ROLE_ADMIN'
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles :' 
            ])
            ->add('firstname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Prénom :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner un prénom.'
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Nom :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner un nom de famille.'
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
