<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Categorie;
use App\Entity\User;
use App\Form\AddCategorieType;
use App\Form\UploadPictureType;
use App\Form\EditPictureType;
use App\Form\UploadUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();
        $resultsCategories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $resultsUsers = $this->getDoctrine()->getRepository(User::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );
        $categories = $paginator->paginate(
            $resultsCategories, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );
        $users = $paginator->paginate(
            $resultsUsers, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );

        return $this->render('admin/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories,
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/categorie/new", name="admin_new_categorie")
     */
    public function newCategorie(Request $request): Response
    {
        $categorie = new Categorie();

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formAdd = $this->createForm(AddCategorieType::class, $categorie);
        $formAdd->handleRequest($request);


        // Vérifie si le formulaire est envoyé et valide :
        if($formAdd->isSubmitted() && $formAdd->isValid()) {
            $categorie->setSlug((new AsciiSlugger())->slug(strtolower($categorie->getName())));

            // Insertion en bdd
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($categorie);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Catégorie ajoutée.');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/categorie/new.html.twig', [
            'formAdd' => $formAdd->createView()
        ]); 
    }

    /**
     * @Route("/admin/categorie/edit/{id}", name="admin_edit_categorie")
     */
    public function editCategorie(Request $request, $id): Response
    {
        $categorie = new Categorie();
        $doctrine = $this->getDoctrine()->getManager();
        $categorie = $doctrine->getRepository(Categorie::class)->find($id);

        if(!$categorie) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas.');
        }

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formEdit = $this->createForm(AddCategorieType::class, $categorie);
        $formEdit->handleRequest($request);


        // Vérifie si le formulaire est envoyé et valide :
        if($formEdit->isSubmitted() && $formEdit->isValid()) {
            $categorie->setSlug((new AsciiSlugger())->slug(strtolower($categorie->getName())));
            // Insertion en bdd
            $doctrine->persist($categorie);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Catégorie modifiée.');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/categorie/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]); 
    }

    /**
     * @Route("/admin/categorie/delete/{id}", name="admin_categorie_delete")
     */
    public function deleteCategorie($id) : Response
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$categorie) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas.'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($categorie);
        $doctrine->flush();

        // Création d'un message flash
        $this->addFlash('success', 'La catégorie a été supprimée.');

        // Redirection vers la page d'accueil
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/admin/picture/edit/{id}", name="admin_edit_picture")
     */
    public function editPicture(Request $request, $id): Response
    {
        $picture = new Picture();
        $doctrine = $this->getDoctrine()->getManager();
        $picture = $doctrine->getRepository(Picture::class)->find($id);

        if(!$picture) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas.');
        }

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formEdit = $this->createForm(EditPictureType::class, $picture);
        $formEdit->handleRequest($request);


        // Vérifie si le formulaire est envoyé et valide :
        if($formEdit->isSubmitted() && $formEdit->isValid()) {

            // Insertion en bdd
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Image modifiée.');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/picture/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]); 
    }

    /**
     * @Route("/admin/picture/{id}", name="admin_delete_picture")
     */
    public function deletePicture($id) : Response
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas.'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        // Création d'un message flash
        $this->addFlash('success', 'L\'image a été supprimée.');

        // Redirection vers la page d'accueil
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/admin/user/edit/{id}", name="admin_edit_user")
     */
    public function editUser(Request $request, $id): Response
    {
        $user = new User();
        $doctrine = $this->getDoctrine()->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);

        if(!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas.');
        }

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formEdit = $this->createForm(UploadUserType::class, $user);
        $formEdit->handleRequest($request);


        // Vérifie si le formulaire est envoyé et valide :
        if($formEdit->isSubmitted() && $formEdit->isValid()) {

            // Insertion en bdd
            $doctrine->persist($user);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Utilisateur modifié.');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/user/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]); 
    }

    /**
     * @Route("/admin/user/{id}", name="admin_delete_user")
     */
    public function deleteUser($id) : Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas.'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        // Création d'un message flash
        $this->addFlash('success', 'L\'utilisateur a été supprimé.');

        // Redirection vers la page d'accueil
        return $this->redirectToRoute('admin');
    }
}
