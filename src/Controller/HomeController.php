<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'isvalid' => true
            ]);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            12 // Nombre de résultats par page
        );

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, Request $request, PaginatorInterface $paginator) {
        
        // la méthode find renvoi un enregistrement uniquement selon son id
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy(
            ['isvalid' => true,
            'category' => $id]
        );

        if(!$category){
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $query, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            12 // Nombre de résultats par page
        );

        // Si l'article existe nous envoyons les données à la vue
        return $this->render('home/picturesByCategory.html.twig', [
            'categorie' => $category,
            'pictures' => $pictures
        ]);

    }

    /**
     * @Route("/picture/{id}", name="picture", requirements={"id"="\d+"})
     */
    public function picture($id, Request $request, PaginatorInterface $paginator) {
        
        // la méthode find renvoi un enregistrement uniquement selon son id
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if(!$picture){
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException('L\'image n\'existe pas...');
        }

        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategory()->getPictures(),
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            12 // Nombre de résultats par page
        );

        // Si l'article existe nous envoyons les données à la vue
        return $this->render('home/picture.html.twig', [
            'picture' => $picture,
            'photos' => $photos
        ]);

    }
}