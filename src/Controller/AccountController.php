<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Categorie;
use App\Entity\User;
use App\Form\UploadPictureType;
use App\Form\EditPictureType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;


class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $this->getUser()->getPictures(),
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            12 // Nombre de résultats par page
        );

        return $this->render('account/index.html.twig', [
            'pictures' => $pictures
        ]);
    }

    /**
     * @Route("/account/new", name="account_new_picture")
     */
    public function new(Request $request): Response
    {
        $picture = new Picture();

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formUpload = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'new_picture'
        ]);
        $formUpload->handleRequest($request);


        // Vérifie si le formulaire est envoyé et valide :
        if($formUpload->isSubmitted() && $formUpload->isValid()) {
            // Ajoute la date du jour de manière automatique
            $picture->setCreatedAt(new \DateTimeImmutable());
            $picture->setIsvalid(false);
            // Passe l'utilisateur actuellement connecté à notre setter
            $picture->setUser($this->getUser());

            // Insertion en bdd
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Merci pour votre partage !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }


        // Si vous voulez capter toutes les erreurs d'un formulaire pour les afficher
        // où vous le souhaitez.
        // if ($formUpload->isSubmitted() && !$formUpload->isValid()) {
        //     $errors = $formUpload->getErrors(true);
        // }

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView()
        ]); 
    }

    /**
     * @Route("/account/edit/{id}", name="edit_picture")
     */
    public function update(Request $request, $id): Response
    {
        $doctrine = $this->getDoctrine()->getManager();
        $picture = $doctrine->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formEdit = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture'
        ]);
        $formEdit->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide :
        // si oui, modifie l'entité
        if($formEdit->isSubmitted() && $formEdit->isValid()) {
            $picture->setUpdatedAt(new \DateTimeImmutable());

            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Votre modification a été enregistrée.');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }

        return $this->render('account/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]); 
        
    }

    /**
     * @Route("/account/delete/picture/{id}", name="delete_picture")
     */
    public function delete($id) : Response
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        // Création d'un message flash
        $this->addFlash('success', 'La photo a été supprimée.');

        // Redirection vers la page d'accueil
        return $this->redirectToRoute('account');
    }
}
