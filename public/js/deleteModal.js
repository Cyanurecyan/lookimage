/**
 * deleteModal.js
 */

window.onload = function() {
   // écouteur d'événement sur les boutons de suppression
   document.querySelectorAll('.confirm-delete').forEach(button => {
      button.addEventListener('click', function(){
         let modal = document.querySelector('#deleteModal');
         let button = modal.querySelector('.btn-danger');
         button.href = this.dataset.href;

         var myModal = new bootstrap.Modal(modal);
         myModal.show();
      });
   });
}